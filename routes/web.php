<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('register', 'AuthController@getRegister');
Route::post('register', 'AuthController@postRegister');
Route::get('login', 'AuthController@getLogin');
Route::post('login', 'AuthController@postLogin');
Route::get('logout', 'AuthController@logout');

Route::middleware(['auth'])->group(function() {
	Route::get('dashboard/patients', 'PatientController@index');
	Route::get('dashboard/patients/doctors/add', 'PatientController@addDoctor');
	Route::post('dashboard/patients/doctors/add', 'PatientController@postAddDoctor');
	Route::get('dashboard/patients/doctors', 'PatientController@listDoctors');
	Route::get('/dashboard/doctors', 'DoctorController@index');
	Route::get('dashboard/doctors/patients/{id}', 'DoctorController@getPatientsMedicalHistory');
	Route::get('dashboard/doctors/patients-{id}/prescription', 'DoctorController@givePrescription');
	Route::get('dashboard/doctors/patients', 'DoctorController@getPatientsList');
	Route::get('dashboard/doctors/consultancy', 'DoctorController@getConsultancyList');
	Route::post('patients-{id}/prescription', 'DoctorController@insertPrescription');

	Route::get('dashboard/patients/doctors/{doctor_id}', 'PatientController@getConsult');
	Route::post('dashboard/patients/doctors/{doctor_id}', 'PatientController@postConsult');

	Route::get('dashboard/patients/pharmacists/add', 'PatientController@addPharmacist');
	Route::post('dashboard/patients/pharmacists/add', 'PatientController@postAddPharmacist');
	Route::get('dashboard/patients/pharmacists', 'PatientController@listPharmacists');
	Route::get('dashboard/patients/pharmacists/{pharmacist_id}', 'PatientController@getRequestMedicine');
	Route::post('dashboard/patients/pharmacists/{pharmacist_id}', 'PatientController@postRequestMedicine');

	Route::get('dashboard/pharmacists', 'PharmacistController@index');
	Route::get('dashboard/pharmacists/delivery', 'PharmacistController@getDeliveries');
	Route::get('dashboard/pharmacists/delivery/{prescription_id}', 'PharmacistController@acceptDelivery');
});

