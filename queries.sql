DROP TABLE IF EXISTS `patient_doctor_mappings`;

DROP TABLE IF EXISTS `prescriptions`;

DROP TABLE IF EXISTS `patients`;

DROP TABLE IF EXISTS `doctors`;

DROP TABLE IF EXISTS `pharmacists`;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`login` varchar(500) NOT NULL,
	`password` varchar(500) NOT NULL,
	`remember_token` varchar(1000) DEFAULT NULL,
	UNIQUE KEY `login` (`login`),
	PRIMARY KEY (`id`)
);

CREATE TABLE `patients` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`name` varchar(500) NOT NULL,
	`gender` enum('m', 'f', 't') DEFAULT 'm',
	`dob` date NOT NULL,
	`address` varchar(2000) NOT NULL,
	`blood_group` enum('A-', 'A+', 'B-', 'B+', 'AB-', 'AB+', 'O-', 'O+') NOT NULL,
	UNIQUE KEY `user_id` (`user_id`),
	PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

CREATE TABLE `doctors` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`name` varchar(500) NOT NULL,
	`gender` enum('m', 'f', 't') DEFAULT 'm',
	`dob` date NOT NULL,
	`address` varchar(2000) NOT NULL,
	`blood_group` enum('A-', 'A+', 'B-', 'B+', 'AB-', 'AB+', 'O-', 'O+') NOT NULL,
	`license_number` varchar(255) NOT NULL,
	UNIQUE KEY `user_id` (`user_id`),
	PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

CREATE TABLE `pharmacists` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`name` varchar(500) NOT NULL,
	`gender` enum('m', 'f', 't') DEFAULT 'm',
	`dob` date NOT NULL,
	`address` varchar(2000) NOT NULL,
	`blood_group` enum('A-', 'A+', 'B-', 'B+', 'AB-', 'AB+', 'O-', 'O+') NOT NULL,
	`pharmacy` varchar(500) NOT NULL,
	`license_number` varchar(255) NOT NULL,
	UNIQUE KEY `user_id` (`user_id`),
	PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

CREATE TABLE `patient_doctor_mappings` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`patient_id` int(11) NOT NULL,
	`doctor_id` int(11) NOT NULL,
	UNIQUE KEY `patient_id_doctor_id` (`patient_id`, `doctor_id`),
	PRIMARY KEY (`id`),
	FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
	FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`)
);

CREATE TABLE `patient_pharmacist_mappings` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`patient_id` int(11) NOT NULL,
	`pharmacist_id` int(11) NOT NULL,
	UNIQUE KEY `patient_id_pharmacist_id` (`patient_id`, `pharmacist_id`),
	PRIMARY KEY (`id`),
	FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
	FOREIGN KEY (`pharmacist_id`) REFERENCES `pharmacists` (`id`)
);

CREATE TABLE `prescriptions` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`patient_id` int(11) NOT NULL,
	`doctor_id` int(11) NOT NULL,
	`symptoms` text NOT NULL,
	`diagnosis` int(11) DEFAULT NULL,
	`prescriptions` text DEFAULT NULL,
	`ambulance_required` tinyint(1) DEFAULT 0,
	`pharmacist_id` int(11) DEFAULT NULL,
	`prescribed` tinyint(1) DEFAULT 0,
	`delivery_accepted` tinyint(1) DEFAULT 0,
	PRIMARY KEY (`id`),
	FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`),
	FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`),
	FOREIGN KEY (`pharmacist_id`) REFERENCES `pharmacists` (`id`)
);

CREATE TABLE `diagnosis` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(100) NOT NULL,
	PRIMARY KEY (`id`)
);