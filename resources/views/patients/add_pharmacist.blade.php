@extends('layouts.app')

@section('sidebar')
    @include('patients.sidebar')
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="header">
                        <h2>
                            Add Pharmacist
                        </h2>
                    </div>
                    <div class="body">
                        <form method="POST" action="{{ url('dashboard/patients/pharmacists/add') }}">
                            @csrf
                            <label for="email_address">Enter Pharmacist ID (Ask your pharmacist for it.)</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="number" name="pharmacist_id" class="form-control" placeholder="Pharmacist ID">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Vertical Layout -->
    </div>
@endsection