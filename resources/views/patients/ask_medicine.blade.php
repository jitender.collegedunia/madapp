@extends('layouts.app')

@section('sidebar')
    @include('patients.sidebar')
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Horizontal Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-primary">
                                <button type="button" aria-hidden="true" class="close js-close" data-dismiss="alert">
                                    <i class="nc-icon nc-simple-remove"></i>
                                </button>
                                <span><b> Success! </b> {{session('status')}}</span>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="card">
                    <div class="header">
                        <h2>
                            Get Consultation
                        </h2>
                    </div>
                    <div class="body">
                        @if ($prescriptions->count() == 0)
                            <span>No prescriptions found</span>
                        @else
                            <form class="form-horizontal" method="POST" action="{{ url('dashboard/patients/pharmacists/'.$pharmacist->id) }}">
                                @csrf
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="prescription_id">Select Prescription</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="prescription_id" id="prescription_id" class="form-control" required>
                                                    <option value="">Select Prescription</option>
                                                    @foreach ($prescriptions as $prescription)
                                                        <option value="{{ $prescription->id }}">{{ $prescription->symptoms }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Request Medicine Delivery</button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Horizontal Layout -->
    </div>
@endsection