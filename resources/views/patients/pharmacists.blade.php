@extends('layouts.app')

@section('sidebar')
    @include('patients.sidebar')
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-primary">
                            <ul>
                                <li><b> Success! </b> {{session('status')}}</li>
                            </ul>
                        </div>
                    @endif
                <div class="card">
                    <div class="header">
                        <h2>
                            Your Pharmacists
                        </h2>
                        <span class="header-dropdown m-r--5">
                            <a class="btn btn-block bg-pink waves-effect" href="{{ url('dashboard/patients/pharmacists/add') }}">Add Pharmacist</a>
                        </span>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Blood Group</th>
                                        <th>Age</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Blood Group</th>
                                        <th>Age</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($pharmacists as $pharmacist)
                                        <tr>
                                            <td>{{ $pharmacist->name }}</td>
                                            <td>{{ $pharmacist->gender == 'm' ? 'Male' : 'Female' }}</td>
                                            <td>{{ $pharmacist->blood_group }}</td>
                                            <td>61</td>
                                            <td>
                                                <a href="{{ url('dashboard/patients/pharmacists/'.$pharmacist->id) }}">Ask Medicines</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
@endsection