
<!DOCTYPE html>
<html>
<head>
<title>TelMD | Making Wellness Contagious</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- This site is optimized with the Yoast SEO plugin v11.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Change the way you think about your health - there is a better way. Start your journey to optimal health today!"/>
<link rel="canonical" href="https://telmd.com/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="TelMD | Making Wellness Contagious" />
<meta property="og:description" content="Change the way you think about your health - there is a better way. Start your journey to optimal health today!" />
<meta property="og:url" content="https://telmd.com/" />
<meta property="og:site_name" content="TelMD" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Change the way you think about your health - there is a better way. Start your journey to optimal health today!" />
<meta name="twitter:title" content="TelMD | Making Wellness Contagious" />
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://telmd.com/#website","url":"https://telmd.com/","name":"TelMD","publisher":{"@id":"https://telmd.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://telmd.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://telmd.com/#webpage","url":"https://telmd.com/","inLanguage":"en-US","name":"TelMD | Making Wellness Contagious","isPartOf":{"@id":"https://telmd.com/#website"},"about":{"@id":"https://telmd.com/#organization"},"datePublished":"2019-02-01T14:42:51+00:00","dateModified":"2019-07-09T17:43:30+00:00","description":"Change the way you think about your health - there is a better way. Start your journey to optimal health today!"}]}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="TelMD &raquo; Homepage Comments Feed" href="https://telmd.com/sample-page/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/telmd.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
display: inline !important;
border: none !important;
box-shadow: none !important;
height: 1em !important;
width: 1em !important;
margin: 0 .07em !important;
vertical-align: -0.1em !important;
background: none !important;
padding: 0 !important;
}
</style>
<link rel='stylesheet' id='css-0-css'  href='https://telmd.com/wp-content/mmr/fee3186e-1562088586.min.css' type='text/css' media='all' />
<link rel='https://api.w.org/' href='https://telmd.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://telmd.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://telmd.com/wp-includes/wlwmanifest.xml" /> 
<link rel='shortlink' href='https://telmd.com/' />
<link rel="alternate" type="application/json+oembed" href="https://telmd.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftelmd.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://telmd.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftelmd.com%2F&#038;format=xml" />
<link rel="apple-touch-icon" sizes="180x180" href="https://telmd.com/wp-content/themes/tm/assets/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="https://telmd.com/wp-content/themes/tm/assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="https://telmd.com/wp-content/themes/tm/assets/images/favicon/favicon-16x16.png">
<link rel="manifest" href="https://telmd.com/wp-content/themes/tm/assets/images/favicon/site.webmanifest">
<link rel="mask-icon" href="https://telmd.com/wp-content/themes/tm/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139063761-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-139063761-1');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '605896386550025'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" 
src="https://www.facebook.com/tr?id=605896386550025&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->    <script src="https://www.google.com/recaptcha/api.js"></script>
</head>
<body class="home page-template page-template-templates page-template-homepage page-template-templateshomepage-php page page-id-2">
<div class="page-wrap">
<header class="header" data-aos="header-animation" data-aos-duration="450" data-aos-delay="150">
<div class="container header__container">
<a class="header__logo" href="https://telmd.com/">
<h1 class="header__logo">
WeCare+
</h1>
</a>

<div class="header__nav-wrap">
<div class="header__nav-inner">
<nav class="header__nav"><ul id="menu-main-nav" class="header__nav-list"><li id="menu-item-1096" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1096"><a href="{{ url('login') }}">Sign In</a></li>
</ul></nav>                </div>
</div>
<a class="btn-default header__mobile_cta" href="{{ url('login') }}">Sign In</a>
<button class="header__btn u-mobile js-mobile-menu-opener">
<span class="header__btn-line"></span>
</button>
</div>
</header>

<div class="page-content-wrap">

<div class="main-banner">
<div class="main-banner__text-holder place-bottom container">
<h1 class="main-banner__title">Your Journey Begins Here</h1>
<div class="main-banner__sub-title"><p><em>Let’s Make Wellness Contagious!™</em></p>
</div>
</div>

<video class="main-banner__video" autoplay loop muted playsinline defaultMuted>
<source src="https://telmd.com/wp-content/uploads/2019/04/video.mp4" type='video/mp4'>
</video>
</div>
<script>(function() {function addEventListener(element,event,handler) {
if(element.addEventListener) {
element.addEventListener(event,handler, false);
} else if(element.attachEvent){
element.attachEvent('on'+event,handler);
}
}function maybePrefixUrlField() {
if(this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
this.value = "http://" + this.value;
}
}

var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
if( urlFields && urlFields.length > 0 ) {
for( var j=0; j < urlFields.length; j++ ) {
addEventListener(urlFields[j],'blur',maybePrefixUrlField);
}
}/* test if browser supports date fields */
var testInput = document.createElement('input');
testInput.setAttribute('type', 'date');
if( testInput.type !== 'date') {

/* add placeholder & pattern to all date fields */
var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
for(var i=0; i<dateFields.length; i++) {
if(!dateFields[i].placeholder) {
dateFields[i].placeholder = 'YYYY-MM-DD';
}
if(!dateFields[i].pattern) {
dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
}
}
}

})();</script><script type='text/javascript'>
/* <![CDATA[ */
var myScriptObject = {"markerImageUrl":"https:\/\/telmd.com\/wp-content\/themes\/tm\/assets\/images\/svg\/marker.png"};var wpcf7 = {"apiSettings":{"root":"https:\/\/telmd.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};var mc4wp_forms_config = [];var mc4wp_ajax_vars = {"loading_character":"\u2022","ajax_url":"https:\/\/telmd.com\/wp-admin\/admin-ajax.php?action=mc4wp-form","error_text":"Oops. Something went wrong. Please try again later."};
/* ]]> */
</script>
<script type='text/javascript' src='https://telmd.com/wp-content/mmr/b073a712-1561654206.min.js'></script>
<!--[if lte IE 9]>
<script type='text/javascript' src='https://telmd.com/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js'></script>
<![endif]-->
</body>
</html>
