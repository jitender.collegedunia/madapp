@extends('layouts.app')

@section('sidebar')
    @include('doctors.sidebar')
@endsection

@section('content')
<form action ="/patients-{{$id}}/prescription" method="post">
    @csrf
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Patients TO Consult</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <tbody>
                                    <tr>
                                        <td> Name :</td>
                                        <td name="p_name" value="$patientDetails->id"> {{ $patientDetails->name }} </td>
                                    </tr>
                                    <tr>
                                        <td> Problem :</td> 
                                        <td  name="p_problem" value="$patientDetails->symptoms">{{ $patientDetails->symptoms }}</td>
                                    </tr>
                                    <tr>
                                        <td>Diagnosis :</td>
                                        <td>
                                            <select name="diagnosis">
                                              <option value="">Select Diagnosis</option>
                                              @foreach ($diagnosisList as $key => $value)
                                                <option value="{{ $value->id }}">{{ $value->title }}</option>
                                              @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Medicines Prescribed </td>
                                        <td><textarea name="prescriptions"></textarea></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button name="save" value="save">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</form>
@endsection