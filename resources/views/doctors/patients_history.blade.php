@extends('layouts.app')

@section('sidebar')
    @include('doctors.sidebar')
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>History</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            @if (empty($patientMedicalHistory))
                                <p> No data found</p>
                            @else
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Problem</th>
                                            <th>Diagnosis</th>
                                            <th>Blood Group</th>
                                            <th>Prescriptions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($patientMedicalHisory as $patient)
                                        <tr>
                                            <td>{{ $patient['patient_problem'] }}</td>
                                            <td>{{ $patient['diagnosis_name'] }}</td>
                                            <td>{{ $patient['blood_group'] }}</td>
                                            <td>{{ $patient['medicine_prescriptions'] }}</td> 
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
@endsection