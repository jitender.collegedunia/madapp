@extends('layouts.app')

@section('sidebar')
    @include('doctors.sidebar')
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>     Patients TO Consult
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            @if ($consultancyList->count() == 0)
                                <p>No prescription requests</p>
                            @else
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Blood Group</th>
                                            <th>DOB</th>
                                            <th>Problem</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Blood Group</th>
                                            <th>DOB</th>
                                            <th>Problem</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($consultancyList as $patient)
                                            <tr>
                                                <td>{{ $patient->name }}</td>
                                                <td>{{ $patient->gender == 'm' ? 'Male' : 'Female' }}</td>
                                                <td>{{ $patient->blood_group }}</td>
                                                <td>27</td>
                                                <td>{{ $patient->symptoms }}</td>
                                                <td class="col-md-1"><a href="/dashboard/doctors/patients-{{ $patient->id }}/prescription"><button>Provide Prescription</button></a></td> 
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
@endsection