<!DOCTYPE html>
<html>

<head>
    @include('snippets.head')
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    @include('snippets.header')
    <section>
        @yield('sidebar')
    </section>

    <section class="content">
        @yield('content')
    </section>

    @include('snippets.footer_scripts')
</body>

</html>
