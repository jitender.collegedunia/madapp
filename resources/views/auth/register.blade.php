<!DOCTYPE html>
<html>

<head>
    @include('snippets.head')
</head>

<body class="signup-page">
    <div class="signup-box">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="logo">
            <a href="javascript:void(0);">WeCare<b>+</b></a>
            <small>HealthCare at your fingertips</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_up" method="POST" action="{{ url('register') }}">
                    @csrf
                    <div class="msg">Register a new membership</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">people_outline</i>
                        </span>
                        <div class="form-line">
                            <select id="profile" name="table" class="form-control" required>
                                <option value="">Select Profile</option>
                                <option value="patients">Patient</option>
                                <option value="doctors">Doctor</option>
                                <option value="pharmacists">Pharmacist</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Your Name" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="login" placeholder="Email/Phone Number" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">people_outline</i>
                        </span>
                        <div class="form-line">
                            <select name="gender" class="form-control" required>
                                <option value="m">Male</option>
                                <option value="f">FeMale</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="date" class="form-control" name="dob" placeholder="Email/Phone Number" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person_pin_circle</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="address" placeholder="Full Address" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">opacity</i>
                        </span>
                        <div class="form-line">
                            <select name="blood_group" class="form-control" required>
                                <option value="A-">A-</option>
                                <option value="A+">A+</option>
                                <option value="B-">B-</option>
                                <option value="B+">B+</option>
                                <option value="AB-">AB-</option>
                                <option value="AB+">AB+</option>
                                <option value="O-">O-</option>
                                <option value="O+">O+</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-group" id="pharmacy_field">
                        <span class="input-group-addon">
                            <i class="material-icons">person_pin_circle</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="pharmacy" placeholder="Pharmacy Name">
                        </div>
                    </div>
                    <div class="input-group" id="license_field">
                        <span class="input-group-addon">
                            <i class="material-icons">person_pin_circle</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="license_number" placeholder="Enter your practice License number">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">SIGN UP</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="{{ url('login') }}">You already have a membership?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/examples/sign-up.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#pharmacy_field').hide();
            $('#license_field').hide();
        });
        $('#profile').change(function() {
            if (this.value == 'pharmacists')
                $('#pharmacy_field').show();
            else
                $('#pharmacy_field').hide();

            if (this.value != 'patients')
                $('#license_field').show();
            else
                $('#license_field').hide();
        });
    </script>
</body>

</html>
