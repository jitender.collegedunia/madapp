<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Helpers\AuthHelper;

class Doctor extends Model
{
    protected $table;
    protected $fillable;

    public  function __construct(){

    	$this->table = "doctors";
    	$this->fillable = ['name','gender','dob','address','blood_group','license_number'];
    }

    public function getPatientListFromModel(){
    	$authHelper = new AuthHelper();
    	$doctor = $authHelper->getUserProfile();
    	$data = DB::table('patients as p')
    			->join('patient_doctor_mappings as pdm','pdm.patient_id','=','p.id')
    			->select('p.id', 'p.name', 'p.gender', 'p.blood_group', 'p.dob')
    			->where('pdm.doctor_id','=',$doctor['id'])
    			->get(); 
        return $data;
    }

    public function getConsultancyListFromModel(){
    	$authHelper = new AuthHelper();
    	$doctor = $authHelper->getUserProfile();
    	$data = DB::table('patients as p')
    			->join('prescriptions as pr','pr.patient_id','=','p.id')
    			->select('p.id', 'p.name', 'pr.symptoms', 'p.gender', 'p.blood_group')
    			->where('pr.doctor_id','=',$doctor['id'])
    			->where('pr.prescribed','=',0)
    			->get(); 
        return $data;
    }      

    public function getPatientsMedicalHistoryFromModel($id, $first = 0)
    {
    	$authHelper = new AuthHelper();
    	$doctor = $authHelper->getUserProfile();

    	$data = DB::table('patients as p')
    			->join('prescriptions as pr','pr.patient_id', '=', 'p.id')
    			->select('p.name', 'p.blood_group', 'pr.symptoms', 'pr.prescriptions')
                ->where('pr.patient_id', $id)
    			->where('pr.doctor_id', '=', $doctor['id']);

        if ($first) {
            $data = $data->where('pr.prescribed', '=', 0)->first();
        }
        else {
			$data = $data->join('diagnosis as d', 'd.id', '=', 'pr.diagnosis')->where('pr.prescribed', '=', 1)->get()->toArray();
        }

        return $data;
    }      

	public function getDiagnosisList(){
		$authHelper = new AuthHelper();
    	$doctor = $authHelper->getUserProfile();
    	$data = DB::table('diagnosis')
    			->get();
    	return $data;
	}    
}
