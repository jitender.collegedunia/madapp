<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\AuthHelper;

use Auth;
use Validator;

class AuthController extends Controller
{
	public function __construct()
	{
		$this->authHelper = new AuthHelper();
	}

    public function getRegister()
    {
    	return view('auth.register');
    }

    public function postRegister(Request $request)
    {
        try {
            $input = $request->input();

            $validator = $this->authHelper->validateRegister($input);

            if ($validator->fails())
                return redirect('register')->withErrors($validator)->withInput();

            if ($this->authHelper->register($input)) {
                return redirect('dashboard/'.$input['table']);
            } else {
                $rules['some_non_existent_field'] = 'required';
                $validator = Validator::make($input, $rules, [
                    'some_non_existent_field.required' => 'Invalid Request. Try again.'
                ]);

                return redirect('register')->withErrors($validator)->withInput();
            }
        } catch(\Exception $e) {
            $rules['some_non_existent_field'] = 'required';
            $validator = Validator::make($input, $rules, [
                'some_non_existent_field.required' => 'Invalid Request. Try again.'
            ]);

            return redirect('register')->withErrors($validator)->withInput();
        }
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $input = $request->input();
        try {
            if (Auth::attempt(['login' => $input['login'], 'password' => $input['password']])) {
                $role = $this->authHelper->getRole();

                if (is_array($role))
                    return redirect('dashboard/'.$role['type']);

            }
            $rules['some_non_existent_field'] = 'required';
            $validator = Validator::make($input, $rules, [
                'some_non_existent_field.required' => 'Login failed.'
            ]);

            return redirect('login')->withErrors($validator)->withInput();

        } catch(\Exception $e) {
            $rules['some_non_existent_field'] = 'required';
            $validator = Validator::make($input, $rules, [
                'some_non_existent_field.required' => 'Login failed.'
            ]);

            return redirect('login')->withErrors($validator)->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
