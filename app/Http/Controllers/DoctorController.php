<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Doctor;
use App\Model\Patient;
use App\Helpers\AuthHelper;

use DB;

class DoctorController extends Controller
{
    public function index(){

    	$doctorId = auth()->user()->id;
    	return view('doctors.dashboard'); 
    }

    public function getPatientsList(){
    	$doctorModel = new Doctor();
    	$patientsList = $doctorModel->getPatientListFromModel(); 
    	return view('doctors.patients', compact('patientsList'));	
    }

    public function getConsultancyList()
    {
    	$doctorModel = new Doctor();
    	$consultancyList = $doctorModel->getConsultancyListFromModel();
    	return view('doctors.consultancy',compact('consultancyList'));	
    }

    public function getPatientsMedicalHistory($id){
    	$doctorModel = new Doctor();
    	$patientMedicalHisory = $doctorModel->getPatientsMedicalHistoryFromModel($id);
    	return view('doctors.patients_history', compact('patientMedicalHisory'));
    }
    
    Public function givePrescription($id)
    {
    	$doctorModel = new Doctor();
    	$diagnosisList = $doctorModel->getDiagnosisList();
    	$patientMedicalHistory = $doctorModel->getPatientsMedicalHistoryFromModel($id, $first = 1);

    	return view('doctors.prescription', [
    		'diagnosisList' => $diagnosisList,
    		'patientDetails' => $patientMedicalHistory,
            'id' => $id
    	]);
    }

    public function insertPrescription(request $request, $patient_id)
    {
    	$medical_data = $request->all();

        $this->authHelper = new AuthHelper();
        $doctor = $this->authHelper->getUserProfile();

    	DB::table('prescriptions')->where(['patient_id' => $patient_id, 'doctor_id' => $doctor['id']])->update(['diagnosis' => $medical_data['diagnosis'], 'prescriptions' => $medical_data['prescriptions'], 'prescribed' => 1]);
    	return redirect('dashboard/doctors/consultancy');
    }
}
