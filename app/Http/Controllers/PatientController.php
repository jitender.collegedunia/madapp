<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Patient;
use App\Helpers\PatientHelper;

class PatientController extends Controller
{
	public function __construct()
	{
		$this->patientModel = new Patient();
		$this->patientHelper = new PatientHelper();
	}
    public function index()
    {
    	$patientId = auth()->user()->id;

    	return view('patients.dashboard');
    }

    public function addDoctor()
    {
        return view('patients.add_doctor');
    }

    public function postAddDoctor(Request $request)
    {
        $input = $request->input();

        $validator = $this->patientHelper->validateAddDoctor($input);

        if ($validator['error'])
            return redirect('dashboard/patients/doctors/add')->withErrors($validator['msg']);

        $this->patientHelper->addDoctor($input);

        return redirect('dashboard/patients/doctors');
    }

    public function listDoctors()
    {
        $doctors = $this->patientHelper->getDoctors();

        return view('patients.doctors', compact('doctors'));
    }

    public function getConsult($doctor_id)
    {
        $doctor = $this->patientHelper->getDoctors($doctor_id);

        return view('patients.consult', compact('doctor'));
    }

    public function postConsult(Request $request, $doctor_id)
    {
        $input = $request->input();

        if (isset($input['ambulance_required']) && $input['ambulance_required'] == 'on')
            $input['ambulance_required'] = 1;
        else
            $input['ambulance_required'] = 0;

        $response = $this->patientHelper->saveConsultationRequest($input, $doctor_id);

        if ($response['error']) {
            return redirect()->back()->withErrors($response['msg']);
        } else {
            return redirect('dashboard/patients/doctors')->with('status', $response['msg']);
        }
    }

    public function addPharmacist()
    {
        return view('patients.add_pharmacist');
    }

    public function postAddPharmacist(Request $request)
    {
        $input = $request->input();

        $validator = $this->patientHelper->validateAddPharmacist($input);

        if ($validator['error'])
            return redirect('dashboard/patients/pharmacists/add')->withErrors($validator['msg']);

        $this->patientHelper->addPharmacist($input);

        return redirect('dashboard/patients/pharmacists');
    }

    public function listPharmacists()
    {
        $pharmacists = $this->patientHelper->getPharmacists();

        return view('patients.pharmacists', compact('pharmacists'));
    }

    public function getRequestMedicine($pharmacist_id)
    {
        $pharmacist = $this->patientHelper->getPharmacists($pharmacist_id);
        $prescriptions = $this->patientHelper->getPrescriptions($status = 1);

        return view('patients.ask_medicine', compact('pharmacist', 'prescriptions'));
    }

    public function postRequestMedicine(Request $request, $pharmacist_id)
    {
        $input = $request->input();

        $this->patientHelper->saveMedicineRequest($input, $pharmacist_id);

        return redirect('dashboard/patients/pharmacists');
    }
}
