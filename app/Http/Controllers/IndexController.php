<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\AuthHelper;

class IndexController extends Controller
{
	public function __construct()
	{
		$this->authHelper = new AuthHelper();
	}

    public function index()
    {
    	if (auth()->check()) {
    		$role = $this->authHelper->getRole();

            if (is_array($role))
                return redirect('dashboard/'.$role['type']);
            else
                return view('index');
    	} else {
    		return view('index');
    	}
    }
}
