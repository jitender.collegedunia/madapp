<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\AuthHelper;
use App\Helpers\PharmacistHelper;

use DB;

class PharmacistController extends Controller
{
	public function __construct()
	{
		$this->authHelper = new AuthHelper();
		$this->pharmacistHelper = new PharmacistHelper();
	}

    public function index()
    {
    	return view('pharmacists.dashboard');
    }

    public function getDeliveries()
    {
    	$pharmacist = $this->authHelper->getUserProfile();

    	$deliveries = DB::table('prescriptions')->where('pharmacist_id', $pharmacist['id'])->get();

    	return view('pharmacists.requests', compact('deliveries'));
    }

    public function acceptDelivery($prescription_id)
    {
    	$pharmacist = $this->authHelper->getUserProfile();

    	$status = DB::table('prescriptions')->where(['pharmacist_id' => $pharmacist['id'], 'id' => $prescription_id])->update(['delivery_accepted' => 1]);

    	return redirect()->back()->with('status', 'Delivery accepted. Patient will see status on his dashboard');
    }
}
