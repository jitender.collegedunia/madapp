<?php

namespace App\Helpers;

use Auth;
use DB;
use Hash;
use Validator;

class AuthHelper
{
	public function validateRegister($input)
	{
		return Validator::make($input, [
            'name' => 'required|string',
            'login' => 'required|string|unique:users,login',
            'gender' => 'in:m,f',
            'dob' => 'required|date_format:Y-m-d',
            'address' => 'required|string',
            'blood_group' => 'required|in:A-,A+,B-,B+,AB-,AB+,O-,O+',
            'password' => 'required|string',
            'confirm' => 'required|same:password'
        ]);
	}

	public function register($input)
	{
		try {
			$userId = DB::table('users')
				->insertGetId(
					['login' => $input['login'], 'password' => Hash::make($input['password'])]
				);

			$insert = ['user_id' => $userId, 'name' => $input['name'], 'gender' => $input['gender'], 'dob' => $input['dob'], 'address' => $input['address'], 'blood_group' => $input['blood_group']];

			if ($input['table'] != 'patients')
				$insert['license_number'] = $input['license_number'];

			if ($input['table'] == 'pharmacists')
				$insert['pharmacy'] = $input['pharmacy'];

			$id = DB::table($input['table'])->insertGetId($insert);

			if ($id) {
				if (Auth::attempt(['login' => $input['login'], 'password' => $input['password']]))
					return true;
				else
					return false;
			} else
				return false;
		} catch(\Exception $e) {dd($e);
			return false;
		}
	}

	public function getUserProfile()
	{
		$role = $this->getRole();
		return (array)DB::table($role['type'])->where('user_id', $role['user_id'])->first();
	}

	public function getRole()
	{
		try {
			return (array)DB::select('SELECT * FROM (SELECT "patients" as type, id, user_id FROM patients UNION SELECT "doctors" as type, id, user_id FROM doctors UNION SELECT "pharmacists" as type, id, user_id FROM pharmacists) AS profiles WHERE user_id = '.auth()->user()->id)[0];
		} catch(\Exception $e) {
			return false;
		}
	}
}
?>