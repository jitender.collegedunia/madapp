<?php

namespace App\Helpers;

use App\Helpers\AuthHelper;
use DB;
use Validator;

class PatientHelper
{
	public function __construct()
	{
		$this->authHelper = new AuthHelper();
	}

	public function validateAddDoctor($input)
	{
		$validator = Validator::make($input, [
			'doctor_id' => 'required|exists:doctors,id'
		]);

		if ($validator->fails())
			return ['error' => 1, 'msg' => $validator->errors()->first('doctor_id')];

		$patient = $this->authHelper->getUserProfile();
		$input['patient_id'] = $patient['id'];

		$mapping = DB::table('patient_doctor_mappings')
		->where(['patient_id' => $input['patient_id'], 'doctor_id' => $input['doctor_id']])
		->first();

		if (!is_null($mapping))
			return ['error' => 1, 'msg' => 'Relationship already exists'];
	}

	public function addDoctor($input)
	{
		$patient = $this->authHelper->getUserProfile();
		$input['patient_id'] = $patient['id'];

		DB::table('patient_doctor_mappings')
			->insert(['patient_id' => $patient['id'], 'doctor_id' => $input['doctor_id']]);

		return true;
	}

	public function getDoctors($doctor_id = null)
	{
		$patient = $this->authHelper->getUserProfile();

		$doctors = DB::table('patient_doctor_mappings')
			->join('patients', 'patients.id', '=', 'patient_doctor_mappings.patient_id')
			->join('doctors', 'doctors.id', '=', 'patient_doctor_mappings.doctor_id')
			->select(
				'doctors.id',
				'doctors.name',
				'doctors.gender', 
				'doctors.blood_group'
			)
			->where('patient_id', $patient['id']);

		if (!is_null($doctor_id)) {
			$doctors = $doctors->where('doctor_id', $doctor_id)->first();
		} else {
			$doctors = $doctors->get();
		}

		return $doctors;
	}

	public function saveConsultationRequest($input, $doctor_id)
	{
		$validator = Validator::make($input, [
			'symptoms' => 'required|string',
			'ambulance_required' => 'required|in:0,1'
		]);

		if ($validator->fails())
			return ['error' => 1, 'msg' => $validator->errors()->first()];

		$patient = $this->authHelper->getUserProfile();

		DB::table('prescriptions')
			->insert(
				['patient_id' => $patient['id'], 'doctor_id' => $doctor_id, 'symptoms' => $input['symptoms'], 'ambulance_required' => $input['ambulance_required']]
			);

		return ['error' => 0, 'msg' => 'Request sent to the doctor successfully.'];
	}

	public function validateAddPharmacist($input)
	{
		$validator = Validator::make($input, [
			'pharmacist_id' => 'required|exists:pharmacists,id'
		]);

		if ($validator->fails())
			return ['error' => 1, 'msg' => $validator->errors()->first('pharmacist_id')];

		$patient = $this->authHelper->getUserProfile();
		$input['patient_id'] = $patient['id'];

		$mapping = DB::table('patient_pharmacist_mappings')
		->where(['patient_id' => $input['patient_id'], 'pharmacist_id' => $input['pharmacist_id']])
		->first();

		if (!is_null($mapping))
			return ['error' => 1, 'msg' => 'Relationship already exists'];
	}

	public function addPharmacist($input)
	{
		$patient = $this->authHelper->getUserProfile();
		$input['patient_id'] = $patient['id'];

		DB::table('patient_pharmacist_mappings')
			->insert(['patient_id' => $patient['id'], 'pharmacist_id' => $input['pharmacist_id']]);

		return true;
	}

	public function getPharmacists($pharmacist_id = null)
	{
		$patient = $this->authHelper->getUserProfile();

		$pharmacists = DB::table('patient_pharmacist_mappings')
			->join('patients', 'patients.id', '=', 'patient_pharmacist_mappings.patient_id')
			->join('pharmacists', 'pharmacists.id', '=', 'patient_pharmacist_mappings.pharmacist_id')
			->select(
				'pharmacists.id',
				'pharmacists.name',
				'pharmacists.gender', 
				'pharmacists.blood_group'
			)
			->where('patient_id', $patient['id']);

		if (!is_null($pharmacist_id)) {
			$pharmacists = $pharmacists->where('pharmacist_id', $pharmacist_id)->first();
		} else {
			$pharmacists = $pharmacists->get();
		}

		return $pharmacists;
	}

	public function getPrescriptions($status = null)
	{
		$patient = $this->authHelper->getUserProfile();

		$prescriptions = DB::table('prescriptions')
			->join('patients', 'patients.id', '=', 'prescriptions.patient_id')
			->select(
				'prescriptions.id',
				'prescriptions.symptoms'
			)
			->where('patient_id', $patient['id']);

		if (!is_null($status)) {
			$prescriptions = $prescriptions->where('prescribed', $status)->where('pharmacist_id', null)->get();
		} else {
			$prescriptions = $prescriptions->get();
		}

		return $prescriptions;
	}

	public function saveMedicineRequest($input, $pharmacist_id)
	{
		$validator = Validator::make($input, [
			'prescription_id' => 'required|exists:prescriptions,id'
		]);

		if ($validator->fails())
			return ['error' => 1, 'msg' => $validator->errors()->first()];

		$patient = $this->authHelper->getUserProfile();

		$status = DB::table('prescriptions')->where('id', $input['prescription_id'])
			->update(
				['pharmacist_id' => $pharmacist_id]
			);

		return ['error' => 0, 'msg' => 'Request sent to the pharmacist successfully.'];
	}
}
